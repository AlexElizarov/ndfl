﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ndfl
{
    public interface IWebService
    {
        void setPassword(string password);
        bool isConnectionOK();
        void addToWebQueue(Person man);
        void deleteFromWebQueue();
        void registrateFirm(Firm firm);
        int getOgrn(Firm firm);
        void setRightPassword(string rightPassword);
        void deregistrateFirm(Firm firm);
        string getRegion();
        void payTax(Person man, double sum);
        void writeOffTax(Person man, double sum);
        void setLeftToPay(Person man, double leftSum);
    }
}
