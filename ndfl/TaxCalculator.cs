﻿using System;

namespace ndfl
{
    public class TaxCalculator
    {
        public static double getTax(Person man)
        {
            return calculateTax(man) - getDeduction(man);
        }

        private static double calculateTax(Person man)
        {
            return (man.getSallary() * 0.13);
        }

        public static double getDeduction(Person man)
        {
            if(man.getSallary() >= 350000)
            {
                return 0;
            }
            double deduction = calculateDeduction(man);
            double tax = calculateTax(man);
            if (deduction <= tax)
            {
                return deduction;
            }
            else
            {
                return tax;
            }
        }

        private static double calculateDeduction(Person man)
        {
            double deduction = calculateDeductionForChilds(man);
            deduction += calculateDeductionForCripples(man);
            deduction += calculateDeductionForTreatment(man);
            return deduction;
        }

        private static double calculateDeductionForTreatment(Person man)
        {
            double deduction = 0;
            if (man.getSumOfTreatment() > 0 && man.getSumOfTreatment() < 120000)
            {
                deduction += man.getSumOfTreatment() * 0.13;
            }

            return deduction;
        }

        private static int calculateDeductionForCripples(Person man)
        {
            int deduction = 0;
            for (int i = 0; i < man.getCripplesCount(); i++)
            {
                deduction += 12000;
            }
            return deduction;
        }

        private static int calculateDeductionForChilds(Person man)
        {
            int deduction = 0;
            for (int i = 0; i < man.getChildsCount(); i++)
            {
                if (i < 2)
                {
                    deduction += 1400;
                }
                else
                {
                    deduction += 3000;
                }
            }
            return deduction;
        }
    }
}