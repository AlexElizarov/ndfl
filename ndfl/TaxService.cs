﻿using System;

namespace ndfl
{
    public class TaxService
    {
        private IWebService webService;
        private string password;
        private string paymentErrorMsg;
        private string congratualtions;

        public TaxService(IWebService service)
        {
            this.webService = service;
        }

        public void connect()
        {
            webService.setPassword(password);
        }

        public void setPassword(string password)
        {
            this.password = password;
        }

        public bool isConnectionOk()
        {
            return webService.isConnectionOK();
        }

        public void addToWebQueue(Person man)
        {
            webService.addToWebQueue(man);
        }

        public void deleteFromWebQueue()
        {
            webService.deleteFromWebQueue();
        }

        public void registrateFirm(Firm firm)
        {
            webService.registrateFirm(firm);
        }

        public void deregistrateFirm(Firm firm)
        {  
            webService.deregistrateFirm(firm);
        }

        public int getOgrn(Firm firm)
        {
            return webService.getOgrn(firm);
        }

        public int getRate()
        {
            if (webService.getRegion() == "Tatarstan")
            {
                return 10;
            }
            else
            {
                return 13;
            }
        }

        public void payTax(Person man, double sum)
        {
            if (sum < TaxCalculator.getTax(man))
            {

                webService.writeOffTax(man, sum);
                paymentErrorMsg = "Too small sum!";
                congratualtions = String.Empty;
            }
            else
            {
                webService.payTax(man, TaxCalculator.getTax(man));
                paymentErrorMsg = String.Empty;
                congratualtions = "Congratulations!";
            }
        }

        public string getErrorMsg()
        {
            return paymentErrorMsg;
        }

        public void writeOffTax(Person man, double sum)
        {
            if(sum < TaxCalculator.getTax(man))
            {
                webService.writeOffTax(man, sum);
                webService.setLeftToPay(man, TaxCalculator.getTax(man) - sum);
                congratualtions = String.Empty;
            }
            else
            {
                webService.writeOffTax(man, TaxCalculator.getTax(man));
                congratualtions = "Congratulations!";
            }
        }

        public string getCongratulations()
        {
            return congratualtions;
        }
    }
}