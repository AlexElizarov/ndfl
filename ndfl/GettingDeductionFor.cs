﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace ndfl
{
    [TestFixture]
    public class GettingDeductionFor
    {
        private Person jack;

        [SetUp]
        public void Init()
        {
            jack = new Person();
        }

        [Test]
        public void manWithOneChildAndBigSallary()
        {
            addSimpleChild();

            jack.setMonthSallary(20000);

            Assert.AreEqual(1400, TaxCalculator.getDeduction(jack));
        }

        [Test]
        public void manWithOneChildAndSmallSallary()
        {
            addSimpleChild();

            jack.setMonthSallary(10000);

            Assert.AreEqual(10000*0.13, TaxCalculator.getDeduction(jack));
        }

        [Test]
        public void manWithTwoChilds()
        {
            addSimpleChild();
            addSimpleChild();

            jack.setMonthSallary(40000);

            Assert.AreEqual(2800, TaxCalculator.getDeduction(jack));
        }

        [Test]
        public void manWithThreeChilds()
        {
            addSimpleChild();
            addSimpleChild();
            addSimpleChild();

            jack.setMonthSallary(50000);

            Assert.AreEqual(5800, TaxCalculator.getDeduction(jack));
        }

        [Test]
        public void manWithCrippleChild()
        {
            addCrippleChild();

            jack.setMonthSallary(100000);

            Assert.AreEqual(12000, TaxCalculator.getDeduction(jack));
        }

        [Test]
        public void manWithAdultChild()
        {
            addAdultChild();

            jack.setMonthSallary(100000);

            Assert.AreEqual(0, TaxCalculator.getDeduction(jack));
        }

        [Test]
        public void manWithAdultStudent()
        {
            addAdultStudent(18);

            jack.setMonthSallary(100000);

            Assert.AreEqual(1400, TaxCalculator.getDeduction(jack));
        }

        [Test]
        public void manWithTooAdultStudent()
        {
            addAdultStudent(24);

            jack.setMonthSallary(100000);

            Assert.AreEqual(0, TaxCalculator.getDeduction(jack));
        }

        [Test]
        public void manWithTreatment()
        {
            jack.setSumOfTreatment(10000);

            jack.setMonthSallary(100000);

            Assert.AreEqual(10000*0.13, TaxCalculator.getDeduction(jack));
        }

        [Test]
        public void manWithTooBigTreatment()
        {
            jack.setSumOfTreatment(200000);

            jack.setMonthSallary(100000);

            Assert.AreEqual(0, TaxCalculator.getDeduction(jack));
        }

        [Test]
        public void manWithTooBigSallary()
        {
            addSimpleChild();
            addSimpleChild();
            addSimpleChild();
            addCrippleChild();
            jack.setSumOfTreatment(100000);

            jack.setMonthSallary(350000);

            Assert.AreEqual(0, TaxCalculator.getDeduction(jack));
        }

        private void addAdultStudent(int age)
        {
            jack.addChild(age, true);
        }

        private void addAdultChild()
        {
            jack.addChild(19, false);
        }

        private void addCrippleChild()
        {
            jack.addCripple(13, false);
        }

        private void addSimpleChild()
        {
            jack.addChild(13, false);
        }
    }
}
