﻿using System;

namespace ndfl
{
    public class Person
    {
        private int sallary;
        private int childs;
        private int cripples;
        private int sumOfTreatment;

        public void setMonthSallary(int sallary)
        {
            this.sallary = sallary;
        }

        public double getSallary()
        {
            return sallary;
        }

        public int getChildsCount()
        {
            return childs;
        }

        public void addChild(int age, bool isStudent)
        {
            if (checkAge(age, isStudent))
            {
                childs++;
            }
        }

        public void addCripple(int age, bool isStudent)
        {
            if (checkAge(age, isStudent))
            {
                cripples++;
            }
        }

        private static bool checkAge(int age, bool isStudent)
        {
            return (age < 18) || (age < 24 && isStudent);
        }

        public int getCripplesCount()
        {
            return cripples;
        }

        public void setSumOfTreatment(int sumOfTreatment)
        {
            this.sumOfTreatment = sumOfTreatment;
        }

        public int getSumOfTreatment()
        {
            return sumOfTreatment;
        }
    }
}