﻿using System;

namespace ndfl
{
    public class Firm
    {
        private string name;

        public Firm(string name)
        {
            this.name = name;
        }

        public string getName()
        {
            return name;
        }
    }
}