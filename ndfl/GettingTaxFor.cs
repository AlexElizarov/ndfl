﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace ndfl
{
   
    [TestFixture]
    public class GettingTaxFor
    {
        private Person jack;

        [SetUp]
        public void Init()
        {
            jack = new Person();
        }

        [Test]
        public void simpleMen()
        {
            jack.setMonthSallary(10000);

            Assert.AreEqual(10000*0.13, TaxCalculator.getTax(jack));
        }

        [Test]
        public void menWithFreeChilds()
        {
            addSimpleChild();
            addSimpleChild();
            addSimpleChild();

            jack.setMonthSallary(100000);

            Assert.AreEqual(100000 * 0.13 - 5800, TaxCalculator.getTax(jack));
        }

        private void addSimpleChild()
        {
            jack.addChild(13, false);
        }
    }
}
