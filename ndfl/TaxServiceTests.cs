﻿using System;
using NUnit.Framework;
using Moq;

namespace ndfl
{
    [TestFixture]
    public class TaxServiceTests
    {
        // State test
        [Test]
        public void connectToWebService_RightPassword_OK()
        {
            TaxService taxService = createStubForConnection(true, "1234");
            taxService.setPassword("1234");

            taxService.connect();

            bool isConnectionOk = taxService.isConnectionOk();
            Assert.True(isConnectionOk);
        }

        // State test
        [Test]
        public void connectToWebService_WrongPassword_NotOK()
        {
            TaxService taxService = createStubForConnection(false, "4321");
            taxService.setPassword("4321");

            taxService.connect();

            bool isConnectionOk = taxService.isConnectionOk();
            Assert.False(isConnectionOk);
        }

        private static TaxService createStubForConnection(bool isConnectionOk, string password)
        {
            var webServiceStub = new Mock<IWebService>();
            webServiceStub.Setup(ws => ws.setRightPassword("1234"));
            webServiceStub.Setup(ws => ws.setPassword(password));
            webServiceStub.Setup(ws => ws.isConnectionOK()).Returns(isConnectionOk);
            TaxService taxService = new TaxService(webServiceStub.Object);
            return taxService;
        }

        // State test
        [Test]
        public void getRate_Tatarstan_10Persent()
        {
            TaxService taxService = createStubForRate("Tatarstan");

            int rate = taxService.getRate();

            Assert.AreEqual(10, rate);
        }

        // State test
        [Test]
        public void getRate_SimpleRegion_13Persent()
        {
            TaxService taxService = createStubForRate("Nizhniy Novgorod region");

            int rate = taxService.getRate();

            Assert.AreEqual(13, rate);
        }

        private static TaxService createStubForRate(string region)
        {
            var webServiceStub = new Mock<IWebService>();
            webServiceStub.Setup(ws => ws.getRegion()).Returns(region);
            TaxService taxService = new TaxService(webServiceStub.Object);
            return taxService;
        }

        // Behavior test?
        [Test]
        public void connectToWebService_EmptyPassword_EmptyPasswordExceptionThrown()
        {
            var webServiceMock = new Mock<IWebService>();
            webServiceMock.Setup(ws => ws.setPassword(String.Empty)).Throws<EmptyPasswordException>();
            TaxService taxService = new TaxService(webServiceMock.Object);
            taxService.setPassword(String.Empty);

            Assert.Throws<EmptyPasswordException>(() => taxService.connect());
        }

        // Behavior test?
        [Test]
        public void connectToWebService_WithoutPassword_NullReferenceExceptionException()
        {
            var webServiceMock = new Mock<IWebService>();
            webServiceMock.Setup(ws => ws.setPassword(null)).Throws <NullReferenceException> ();
            TaxService taxService = new TaxService(webServiceMock.Object);
            taxService.setPassword(null);

            Assert.Throws<NullReferenceException>(() => taxService.connect());
        }

        // Behavior test
        [Test]
        public void addToWebQueue_OnePerson_addMethodCallsOnce()
        {
            var webServiceMock = new Mock<IWebService>();
            TaxService taxService = new TaxService(webServiceMock.Object);

            taxService.addToWebQueue(createPerson());

            webServiceMock.Verify(ws => ws.addToWebQueue(It.IsAny<Person>()), Times.Once());
        }

        // Behavior test
        [Test]
        public void addToWebQueue_TwoPerson_addMethodCallsTwice()
        {
            var webServiceMock = new Mock<IWebService>();
            TaxService taxService = new TaxService(webServiceMock.Object);

            taxService.addToWebQueue(createPerson());
            taxService.addToWebQueue(createPerson());

            webServiceMock.Verify(ws => ws.addToWebQueue(It.IsAny<Person>()), Times.Exactly(2));
        }

        // Behavior test
        [Test]
        public void deleteFromWebQueue_QueueWithOnePerson_deleteMethodCallsOnce()
        {
            var webServiceMock = new Mock<IWebService>();
            TaxService taxService = new TaxService(webServiceMock.Object);
            taxService.addToWebQueue(createPerson());

            taxService.deleteFromWebQueue();

            webServiceMock.Verify(ws => ws.deleteFromWebQueue(), Times.Once());
        }

        // Behavior test?
        [Test]
        public void deleteFromWebQueue_EmptyQueue_EmptyQueueExceptionThrown()
        {
            var webServiceMock = new Mock<IWebService>();
            webServiceMock.Setup(ws => ws.deleteFromWebQueue()).Throws<EmptyQueueException>();
            TaxService taxService = new TaxService(webServiceMock.Object);

            Assert.Throws<EmptyQueueException>(() => taxService.deleteFromWebQueue());
        }

        // Behavior test
        [Test]
        public void registrateFirm_OneFirm_RegistrateMetgodCallsOnce()
        {
            var webServiceMock = new Mock<IWebService>();
            TaxService taxService = new TaxService(webServiceMock.Object);
           
            taxService.registrateFirm(createFirm());

            webServiceMock.Verify(ws => ws.registrateFirm(It.IsAny<Firm>()), Times.Once());
        }

        // Behavior test
        [Test]
        public void registrateFirm_TwoFirm_RegistrateMetgodCallsTwice()
        {
            var webServiceMock = new Mock<IWebService>();
            TaxService taxService = new TaxService(webServiceMock.Object);

            taxService.registrateFirm(new Firm("Firm"));
            taxService.registrateFirm(new Firm("Firm2"));

            webServiceMock.Verify(ws => ws.registrateFirm(It.IsAny<Firm>()), Times.Exactly(2));
        }

        // Behavior test?
        [Test]
        public void getOgrn_UnknownFirm_UnknownFirmExceptionThrown()
        {
            var webServiceMock = new Mock<IWebService>();
            webServiceMock.Setup(ws => ws.getOgrn(It.IsAny<Firm>())).Throws<UnknownFirmException>();
            TaxService taxService = new TaxService(webServiceMock.Object);

            Assert.Throws<UnknownFirmException>(() => taxService.getOgrn(createFirm()));
        }

        // Behavior test?
        [Test]
        public void getOgrn_DeregistratedFirm_UnknownFirmExceptionThrown()
        {
            var webServiceMock = new Mock<IWebService>();
            TaxService taxService = new TaxService(webServiceMock.Object);
            Firm firm = createFirm();
            taxService.registrateFirm(firm);
            taxService.deregistrateFirm(firm);
            webServiceMock.Setup(ws => ws.getOgrn(firm)).Throws<UnknownFirmException>();

            Assert.Throws<UnknownFirmException>(() => taxService.getOgrn(firm));
        }

        // Behavior test?
        [Test]
        public void deregistrateFirm_UnknownFirm_UnknownFirmExceptionThrown()
        {
            var webServiceMock = new Mock<IWebService>();
            webServiceMock.Setup(ws => ws.deregistrateFirm(It.IsAny<Firm>())).Throws<UnknownFirmException>();
            TaxService taxService = new TaxService(webServiceMock.Object);

            Assert.Throws<UnknownFirmException>(() => taxService.deregistrateFirm(createFirm()));
        }

        // Behavior test
        [Test]
        public void payTax_RightSum_PayMethodCallsOnce()
        {
            var webServiceMock = new Mock<IWebService>();
            TaxService taxService = new TaxService(webServiceMock.Object);
            Person jack = createPersonWithSallary100000();

            taxService.payTax(jack, getTax(jack));

            webServiceMock.Verify(ws => ws.payTax(jack, TaxCalculator.getTax(jack)), Times.Once());
        }

        // Behavior test
        [Test]
        public void payTax_TooSmallSum_PayMethodCallsNever()
        {
            var webServiceMock = new Mock<IWebService>();
            TaxService taxService = new TaxService(webServiceMock.Object);
            Person jack = createPersonWithSallary100000();

            taxService.payTax(jack, getTooSmallTax(jack));

            webServiceMock.Verify(ws => ws.payTax(It.IsAny<Person>(), It.IsAny<double>()), Times.Never());
        }

        // State test
        [Test]
        public void payTax_TooSmallSum_ErrorMessage()
        {
            var webServiceMock = new Mock<IWebService>();
            TaxService taxService = new TaxService(webServiceMock.Object);
            Person jack = createPersonWithSallary100000();

            taxService.payTax(jack, getTooSmallTax(jack));

            string errorMsg = taxService.getErrorMsg();
            Assert.AreEqual("Too small sum!", errorMsg);
        }

        // Behavior test
        [Test]
        public void writeOffTax_NotAllTax_TaxWriteOffAndSetLeftToPayMethodsCall()
        {
            var webServiceMock = new Mock<IWebService>();
            Person jack = createPersonWithSallary100000();
            webServiceMock.Setup(ws => ws.setLeftToPay(jack, getTooSmallTax(jack)));
            webServiceMock.Setup(ws => ws.writeOffTax(jack, 1));
            TaxService taxService = new TaxService(webServiceMock.Object);

            taxService.writeOffTax(jack, 1);

            webServiceMock.VerifyAll();
        }

        // Behavior test
        [Test]
        public void writeOffTax_AllTax_setLeftToPayMethodCallsNever()
        {
            var webServiceMock = new Mock<IWebService>();
            Person jack = createPersonWithSallary100000();
            TaxService taxService = new TaxService(webServiceMock.Object);

            taxService.writeOffTax(jack, getTax(jack));

            webServiceMock.Verify(ws => ws.setLeftToPay(It.IsAny<Person>(), It.IsAny<double>()), Times.Never());
        }

        // Behavior test
        [Test]
        public void writeOffTax_AllTax_writeOffTaxMethodCallsOnce()
        {
            var webServiceMock = new Mock<IWebService>();
            Person jack = createPersonWithSallary100000();
            TaxService taxService = new TaxService(webServiceMock.Object);

            taxService.writeOffTax(jack, getTax(jack));

            webServiceMock.Verify(ws => ws.writeOffTax(jack, getTax(jack)), Times.Once());
        }

        // Behavior test
        [Test]
        public void payTax_NotAllTax_setLeftToPayMethodCallsNever()
        {
            var webServiceMock = new Mock<IWebService>();
            Person jack = createPersonWithSallary100000();
            TaxService taxService = new TaxService(webServiceMock.Object);

            taxService.payTax(jack, getTooSmallTax(jack));

            webServiceMock.Verify(ws => ws.setLeftToPay(It.IsAny<Person>(), It.IsAny<double>()), Times.Never());
        }

        // State test
        [Test]
        public void payTax_RightSumAfterWrongSum_NoErrorMessages()
        {
            TaxService taxService = createStubForPayments();
            Person jack = createPersonWithSallary100000();
            taxService.payTax(jack, getTooSmallTax(jack));
            taxService.payTax(jack, getTax(jack));

            string errorMsg = taxService.getErrorMsg();
            Assert.AreEqual(String.Empty, errorMsg);
        }

        // State test
        [Test]
        public void payTax_RightSum_Congratulations()
        {
            TaxService taxService = createStubForPayments();
            Person jack = createPersonWithSallary100000();

            taxService.payTax(jack, getTax(jack));

            string congratulations = taxService.getCongratulations();
            Assert.AreEqual("Congratulations!", congratulations);
        }

        // State test
        [Test]
        public void payTax_TooSmallSum_NoCongratulations()
        {
            TaxService taxService = createStubForPayments();
            Person jack = createPersonWithSallary100000();

            taxService.payTax(jack, getTooSmallTax(jack));

            string congratulations = taxService.getCongratulations();
            Assert.AreEqual(String.Empty, congratulations);
        }

        // State test
        [Test]
        public void writeOffTax_RightSum_Congratulations()
        {
            TaxService taxService = createStubForPayments();
            Person jack = createPersonWithSallary100000();

            taxService.writeOffTax(jack, getTax(jack));

            string congratulations = taxService.getCongratulations();
            Assert.AreEqual("Congratulations!", congratulations);
        }

        // State test
        [Test]
        public void writeOffTax_TooSmallSum_NoCongratulations()
        {
            TaxService taxService = createStubForPayments();
            Person jack = createPersonWithSallary100000();

            taxService.writeOffTax(jack, getTooSmallTax(jack));

            string congratulations = taxService.getCongratulations();
            Assert.AreEqual(String.Empty, congratulations);
        }

        // State test
        [Test]
        public void payTax_TooBigSum_NoCongratulations()
        {
            TaxService taxService = createStubForPayments();
            Person jack = createPersonWithSallary100000();

            taxService.payTax(jack, getTooBigTax(jack));

            string congratulations = taxService.getCongratulations();
            Assert.AreEqual("Congratulations!", congratulations);
        }

        // State test
        [Test]
        public void writeOffTax_TooBigSum_Congratulations()
        {
            TaxService taxService = createStubForPayments();
            Person jack = createPersonWithSallary100000();

            taxService.writeOffTax(jack, getTooBigTax(jack));

            string congratulations = taxService.getCongratulations();
            Assert.AreEqual("Congratulations!", congratulations);
        }

        private static TaxService createStubForPayments()
        {
            var webServiceStub = new Mock<IWebService>();
            TaxService taxService = new TaxService(webServiceStub.Object);
            return taxService;
        }

        // Behavior test
        [Test]
        public void writeOffTax_TooBigSum_WriteOffOnlyTax()
        {
            var webServiceMock = new Mock<IWebService>();
            TaxService taxService = new TaxService(webServiceMock.Object);
            Person jack = createPersonWithSallary100000();

            taxService.writeOffTax(jack, getTooBigTax(jack));

            webServiceMock.Verify(ws => ws.writeOffTax(jack, TaxCalculator.getTax(jack)));
        }

        // Behavior test
        [Test]
        public void payTax_TooBigSum_payOnlyTax()
        {
            var webServiceMock = new Mock<IWebService>();
            TaxService taxService = new TaxService(webServiceMock.Object);
            Person jack = createPersonWithSallary100000();

            taxService.payTax(jack, getTooBigTax(jack));

            webServiceMock.Verify(ws => ws.payTax(jack, TaxCalculator.getTax(jack)));
        }

        private Firm createFirm()
        {
            return new Firm("firm");
        }

        private Person createPerson()
        {
            return new Person();
        }

        private Person createPersonWithSallary100000()
        {
            Person man = new Person();
            man.setMonthSallary(100000);
            return man;
        }

        private double getTax(Person man)
        {
            return TaxCalculator.getTax(man);
        }

        private double getTooSmallTax(Person man)
        {
            return TaxCalculator.getTax(man)-1;
        }

        private double getTooBigTax(Person man)
        {
            return TaxCalculator.getTax(man) + 1;
        }

    }
}
