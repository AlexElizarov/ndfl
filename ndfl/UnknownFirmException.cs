﻿using System;
using System.Runtime.Serialization;

namespace ndfl
{
    public class UnknownFirmException : Exception
    {
        public UnknownFirmException()
        {
        }

        public UnknownFirmException(string message) : base(message)
        {
        }

        public UnknownFirmException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected UnknownFirmException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}